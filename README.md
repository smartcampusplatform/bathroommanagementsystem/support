# Support 
=============================
1. Modul Water level sensing
2. Modul Water polution sensing
3. Modul Water automatic refill

Untuk water automatic refill sendiri kami satukan dengan water level sensing.

## Water Level Sensing & Water Automatic Refill

1. Jalankan kode water_level_POST.ino
2. Jangan lupa mengganti 'ssid' dan 'password' wifi yang terhubung dengan perangkat
3. Jangan lupa mengganti 'host' dengan server yang dijalankan
4. Untuk volume bak yang berbeda silahkan mengganti nilai variabel 'tinggibak'
5. Untuk mengganti parameter volume minimal bak untuk menyalakan keran secara otomatis silahkan ubah nilai variabel 'minimum'
6. Jangan lupa memperhatikan setiap pin yang terpasang

Alat yang dibutuhkan:

1. 1 buah breadboard
2. 1 buah arduino uno
3. 1 buah actuator/motor keran (untuk proyek ini kami ganti dengan LED terlebih dahulu)
4. 1 buah modul relay
4. 1 buah sensor ultrasonik
5. 1 buah modul wifi esp8266-01 ex
6. 1 buah kabel usb
7. jumper male to male
8. jumper male to female
8. 1 buah resistor 220 ohm

Setup alat:
1. sensor ultrasonik
    a. Gnd ke ground
    b. Vcc ke 5V
    c. Echo ke pin 12 di arduino uno
    d. Trig ke pin 11 di arduino uno
2. relay + led
    a. Gnd ke ground
    b. Vcc ke 5V
    c. In ke pin 2 di arduino uno
    d. C ke pin 10 di arduino uno
    e. NC ke LED positif (yang lebih panjang), namun lebih baik menggunakan resistor 220 ohm dulu / tidak langsung ke LED
    f. LED negatif (yang lebih pendek) ke ground
3. esp
    a. 3.3V ke 3.3V
    b. RX ke pin RX atau pin 0 di arduino uno
    c. IO0 / GPIO0 ke ground
    d. RST (tidak usah dihubungkan kemana2)
    e. EN ke 3.3V
    f. IO2 / GPIO2 (tidak usah dihubungkan kemana2)
    g. TX ke pin TX atau pin 1 di arduino uno
    h. GND ke ground
    i. pin reset pada arduino harap dihubungkan ke ground

karena pin 5v, 3.3v, dan ground pada arduino terbatas silahkan gunakan breadboard, jangan lupa memperhatikan sifat aliran di breadboard
[(http://tamrinmedia.blogspot.com/2015/08/cara-penggunaan-breadboard.html)]


## Developed by:
    1. Athur Naufan M
    2. Akbar Ghifari
    3. Qonita Salamah A
    4. Christabel Lawandy E
