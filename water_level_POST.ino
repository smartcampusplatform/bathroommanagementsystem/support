#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

// define pin input pada arduino;
// ultrasonik
int trigPin = 11;
int echoPin = 12;
// relay
int relay_pin = 2;
// led, disini dianggap sebagai pengganti motor keran air
int led_pin = 10;
long tinggibak = 50;
long minimum = 3;
String data;

const char* ssid = "qon"; //jangan lupa diisi sama nama wifi
const char* password = "cintaitbsc"; //jangan lupa diisi sama password

const char *host = "https://api.thingspeak.com/channels/780896/feeds.json?api_key=BLH6MT9WR0TGZCO1";

void setup(){ 
  //memulai serial
  Serial.begin (115200);

  //ulrason
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //relay
  pinMode(relay_pin,OUTPUT);
  pinMode(led_pin,OUTPUT);
  //led  
  digitalWrite(led_pin,HIGH);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Waiting for connection");
    Serial.write("Waiting for connection");
  }
  delay (5000);

}
void loop(){
  // define variabel
  long duration, distance;
  long temp = 0;
  String tinggi, temptinggi;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);

  // konversi jarak
  distance = (duration*340/20000);
  tinggibak = tinggibak - distance;
  Serial.print("tinggi bak: ");
  Serial.print(tinggibak);
  Serial.println(" cm");
  
  if (distance>temp) {
      delay(1000);
      temp = distance;
  } 
  else {
      temp = temp;
  }

  Serial.print("tinggi bak sebelumnya: ");
  Serial.print(temp);
  Serial.println(" cm");

  // relay nyala (otomatis keran/led nyala) ketika jarak sensor 
  // ke benda <=3
  // anggaplah sensor ultrasonik ini disimpan di permukaan atas bak
  // maka keran harusnya hidup bila jarak sensor ke permukaan air
  // lebih dari dari batas minimum yaitu 50% tinggi bak
  if (distance > minimum) {
    digitalWrite(relay_pin,HIGH);
    delay(500);
  }
  else {
    digitalWrite(relay_pin,LOW);
    delay(500);
  }

  tinggi = String(tinggibak);
  temptinggi = String(temp);

  //JSON
  StaticJsonDocument<256> doc;
  JsonObject root = doc.to<JsonObject>();
  root["tinggibak"] = tinggi;
  root["temp"] = temptinggi;
  serializeJson(root, data);
  serializeJson(root, Serial);
  
  delay (5000);  
  
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.begin(host); //jangan lupa diganti sama server punya kalian. //coba.php itu cuma buat ngehandle data yang masuk, terus dimasukin ke database
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    Serial.print("Berhasil konek wifi");
    
    int httpCode = http.POST(data); //ngirim data
    delay(7000);
    String payload = http.getString();

    Serial.println(httpCode);
    Serial.println(payload);

    http.end();
  } else {
    Serial.println("Error connect.");
  }

}