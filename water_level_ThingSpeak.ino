#include "ThingSpeak.h"
#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

//-------- Enter Your Wi-Fi Details -----//
char ssid[] = "GREEN LAND";  //SSID
char pass[] = "951303aja"; //Password
//--------------------------------------//

WiFiClient  client;

unsigned long myChannelField = 123456; // Channel ID
const int ChannelField = 1; // Which To Field Write
const char * myWriteAPIKey = "BLH6MT9WR0TGZCO1"; // Write API Key

String value = "";

// define pin input pada arduino;
  // ultrasonik
  int trigPin = 11;
  int echoPin = 12;
  // relay
  int relay_pin = 2;
  // led, disini dianggap sebagai pengganti motor keran air
  int led_pin = 10; 
  // asumsi tinggi bak adalah 50 cm 
  long tinggibak = 50;
  // define variabel
  long duration, distance;
  long topheight = 0;
void setup()
{
  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  ThingSpeak.begin(client);

  //setup pin hardware
  //ulrason
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //relay
  pinMode(relay_pin,OUTPUT);
  pinMode(led_pin,OUTPUT);
  //led  
  digitalWrite(led_pin,HIGH);
}
void loop()
{
  if (WiFi.status() != WL_CONNECTED)
  {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    while (WiFi.status() != WL_CONNECTED)
    {
      WiFi.begin(ssid, pass);
      delay(5000);
    }
    Serial.println("\nConnected.");
  }

  /*tambahan code loop sensor ultrasonik*/
    digitalWrite(trigPin, LOW);
    delayMicroseconds(3);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
  
    // konversi jarak
    distance = tinggibak - (duration*340/20000);
    topheight = distance;
    Serial.print(distance);
    Serial.println(" cm");
  
    //kalau jarak setelahnya makin pendek / tinggibak makin tinggi maka akan mengganti nilai 
    if (distance>topheight) {
      topheight = distance;
    }
    else {
      topheight = topheight;
    }
    
    // relay nyala (otomatis keran/led nyala) ketika jarak sensor 
    // ke benda <=3
    // anggaplah sensor ultrasonik ini disimpan di permukaan atas bak
    // maka keran harusnya hidup bila jarak sensor ke permukaan air
    // lebih dari dari batas minimum yaitu 50% tinggi bak
    if (distance > 12.5) {
      digitalWrite(relay_pin,HIGH);
      delay(100);
    }
    else {
      digitalWrite(relay_pin,LOW);
      delay(100);
    }
    // Update the 2 ThingSpeak fields with the new data
    ThingSpeak.setField(1, (float)distance);
    ThingSpeak.setField(2, (float)topheight);

    // Write the fields that you've set all at once.
    ThingSpeak.writeFields(myChannelField, myWriteAPIKey);
    delay(1000);
}