// include library untuk esp connect ke wifi dan webserver
#include <stdio.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

/*Put your SSID & Password wifi yang terhubung dengan web browsermu*/
const char* ssid = "GREEN LAND";  // Enter SSID here
const char* password = "951303aja";  //Enter Password here

// state port web server yaitu port 80
/*ESP8266WebServer server(80);*/

//Web Server address to read/write from 
const char *host = "http://arduinojson.org/example.json";

// define pin input pada arduino;
// soil conductivity sensor sebagai penanda polusi air
  int soilPin = A0;
  int temp;
  int soilData1;
  int soilData2;
  int toiletNumber = 112;
  StaticJsonDocument<200> doc;

// Set up the WDT to perform a system reset if the loop() blocks for more than 1 second
void setup() {
  //mulai serial dengan baud 115200 
  Serial.begin(115200);
  delay(100);
  Serial.println();
  
  //disable watchdogtimer
  ESP.wdtDisable();
  
  Serial.println("Connecting to ");
  Serial.println(ssid);

  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");  
  Serial.println(WiFi.localIP());
}

void loop() {
  while (1==1){
    // Check WiFi Status
    if (WiFi.status() == WL_CONNECTED) {
      HTTPClient http;  //Object of class HTTPClient
      http.begin(host);
      int httpCode = http.GET();
      //Check the returning code                                                                  
      if (httpCode > 0) {
        // Get the request response payload
        String payload = http.getString();
        // Parsing
        Serial.print("Response Code:"); //200 is OK
        Serial.println(httpCode);   //Print HTTP return code
       
        Serial.print("Returned data from Server:");
        Serial.println(payload);    //Print request response payload
        
        if(httpCode == 200)
        { 
          //Read from sensor
          Serial.print("Water conductivity: ");
          temp = analogRead(soilPin);
          Serial.println(temp);
        
          //Data Preparation
          soilData1 = temp;
          soilData2 = analogRead(soilPin);

          char json[1024]
          strcat(json, "{\tempPollutionRate\":\"");
          strcat(json, soilData1);
          strcat(json, "\",\"currentPollutionRate\":");
          strcat(json, soilData2);
          strcat(json, ",\"Toilet\":");
          strcat(json, toiletNumber);

          temp = soilData2;
          
          // Allocate JsonBuffer
          // Use arduinojson.org/assistant to compute the capacity.
          JsonObject root = doc.to<JsonObject>();
        
         // Parse JSON object
          deserializeJson(root, payload);
          auto error = deserializeJson(root, payload);
          if (error) {
              Serial.print(F("deserializeJson() failed with code "));
              Serial.println(error.c_str());
              return;
          }
        
          // Decode JSON/Extract values
          Serial.println(F("Response:"));
          Serial.println(root["tempPollutionRate"].as<char*>());
          Serial.println(root["currentPollutionRate"].as<char*>());
          Serial.println(root["toiletNumber"].as<char*>());
        }
        else
        {
          Serial.println("Error in response");
        }
      }
      http.end();   //Close connection
    }
  }
  // Delay
  delay(60000);
}

/*void handle_OnConnect() {
  long jarak = distance;
  server.send(200, "text/html", SendHTML(jarak)); 
}

void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
}*/
