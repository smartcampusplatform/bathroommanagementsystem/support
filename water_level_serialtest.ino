// define pin input pada arduino;
// ultrasonik
int trigPin = 11;
int echoPin = 12;
// relay
int relay_pin = 2;
// led, disini dianggap sebagai pengganti motor keran air
int led_pin = 10;
long tinggibak = 50;

void setup(){ 
  //memulai serial
  Serial.begin (9600);

  //ulrason
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  //relay
  pinMode(relay_pin,OUTPUT);
  pinMode(led_pin,OUTPUT);
  //led  
  digitalWrite(led_pin,HIGH);}
void loop(){
  // define variabel
  long duration, distance;
  long temp = 0;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);

  // konversi jarak
  distance = (duration*340/20000);
  tinggibak = tinggibak - distance;
  Serial.print("tinggi bak: ");
  Serial.print(tinggi bak);
  Serial.println(" cm");
  
  if (distance>temp) {
      delay(1000);
      temp = distance;
  } 
  else {
      temp = temp;
  }

  Serial.print("tinggi bak sebelumnya: ");
  Serial.print(temp);
  Serial.println(" cm");

  // relay nyala (otomatis keran/led nyala) ketika jarak sensor 
  // ke benda <=3
  // anggaplah sensor ultrasonik ini disimpan di permukaan atas bak
  // maka keran harusnya hidup bila jarak sensor ke permukaan air
  // lebih dari dari batas minimum yaitu 50% tinggi bak
  if (distance > 3) {
    digitalWrite(relay_pin,HIGH);
    delay(500);
  }
  else {
    digitalWrite(relay_pin,LOW);
    delay(500);
  }
  
}